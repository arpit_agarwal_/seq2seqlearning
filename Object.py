import cPickle as pkl
import numpy as np


class Object(object):
	name = ''
	 
	def __init__(self, name=''):
		"""
		initializes the dictionaries 
		"""
		f = open('object.dict.pkl','rb')
		self.objects = pkl.load(f)
		self.objSize = len(self.objects.keys())
		f_aff = open('affordances.dict.pkl','rb')
		self.affordances = pkl.load(f_aff)
		self.affSize = len(self.affordances.keys())
		f_state = open('state.dict.pkl','rb')
		self.stateName = pkl.load(f_state)
		self.stateSize = len(self.stateName.keys())	

		# size of object vector (76, 11, 34)
		# print self.objSize, self.affSize, self.stateSize
		
		if len(name):
			self.name = name.lower()
			self.create_obj(name) 
		else: 
			self.obj_vec_sparse = np.zeros(self.objSize+self.affSize+self.stateSize)
		
	def create_obj(self, name):
		"""
		encodes object vectors from name
		"""
		self.name = name.lower()
		objNameVec = np.zeros(self.objSize)
		objAffordance = np.zeros(self.affSize)
		objState = -1*np.ones(self.stateSize)
		if name in self.objects:
			objDict = self.objects[self.name]
			objNameVec[objDict[0]] = 1
			for aff in objDict[1]:
				objAffordance[self.affordances[aff]] = 1
			for state in objDict[2]:
				objState[self.stateName[state[0]]] = 1

		self.obj_vec_sparse = np.concatenate((objNameVec, objAffordance, objState),axis = 0)
		# return obj_vec_sparse

	def get_object_embedding(self):
		"""
		returns object embedding
		"""
		return self.obj_vec_sparse

if __name__ == '__main__':
	# TEST CASE 1
	obj = Object()
	obj.create_obj('mug')
	print obj.get_object_embedding()


