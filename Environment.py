import numpy as np
from Object import Object
from embed_environment import embed_environment
from embed_object import embed_object
import xml.etree.ElementTree as ET

class Environment(object):
	objList = []
	
	def load_environment(self,filename='livingRoom1.xml'):
		"""
		load the environment and calculate environment vector using objects 
		from given file
		"""
		tree = ET.parse(filename)
		root = tree.getroot()
		
		for object_ in root:
			name = object_[0].text
			tmp = name.find('_')
			if tmp != -1:
				name = name[0:tmp]
			self.objList += [Object(name.lower())]
		
		self.calc_env_embedding()

	def set_embed_matrix(self, W=np.array([])):
		"""set matrix to convert sparse object vector to dense matrix
		"""
		if W.size:
			self.W_sparse_to_dense = W

	def get_env_object(self):
		"""
		return list of object vectors
		"""
		return self.objList

	def calc_env_embedding(self):
		"""
		calculate environment embedding from object list
		"""
		from embed_environment import dense_dim

		# assuming there are objects in the environment
		hot_vector_dim = self.objList[0].get_object_embedding().size
		self.W_sparse_to_dense = np.random.randn(dense_dim, hot_vector_dim)
		
		# calculate dense object embedding from objects
		obj_embed_model = embed_object(hot_vector_dim=hot_vector_dim, dense_vector_dim=dense_dim, W=self.W_sparse_to_dense)
		obj_vec_list = []
		for obj in self.objList:
			obj_vec_list +=  [obj_embed_model.calc_object_embedding(obj.get_object_embedding())]

		self.obj_vec_array = np.array(obj_vec_list)
		
		# calculate environment embedding from dense object embedding
		env_embed_model = embed_environment()
		self.env_vec = env_embed_model.calculate_environment_embedding(self.obj_vec_array)

	def get_env_embedding(self):
		"""
		gives environment embedding 
		"""
		return self.env_vec


if __name__ == '__main__':
		env = Environment()
		env.load_environment()
		vec = env.get_env_embedding()
		print vec, vec.shape
