import numpy
import theano
from utils import prepare_data
from theano import config
SEED = 123
numpy.random.seed(SEED)

class Update(object):

    def __init__(self, encoder_model, decoder_model, env_model):
        self.decoder_update_model = Decoder_update(decoder_model) 
        self.encoder_update_model = Encoder_update(encoder_model) 
    
    def get_update(self, star, ground_truth, input_state, encoder_model, decoder_model, env_model):
        """
        updates parameters of decoder, encoder and environment model
        """
        dl_dc_0, star = self.decoder_model.get_update(star, ground_truth, input_state, env_model, decoder_model)
        dl_dh = dl_dc_0[0,:encoder_model.model_options['cell_state_dim']]
        encoder_update_model.get_update(dl_dh, sentence, encode_model)
        return star

class Decoder_update(object):
    
    def __init__(self, decoder_model):
        ## init
        self.input_state = numpy.random.randn((star.shape[0],decoder_model.model_options['cell_state_dim'])).astype(config.floatX)
        
        # parameters to be updated
        self.update_param = OrderedDict()
        self.update_param['lstm_W'] =  numpy.zeros(decoder_model.tparams['lstm_W'].get_value().shape)
        self.update_param['lstm_U'] =  numpy.zeros(decoder_model.tparams['lstm_U'].get_value().shape)
        self.update_param['lstm_b'] =  numpy.zeros(decoder_model.tparams['lstm_b'].get_value().shape)
        self.update_param['star'] =  numpy.zeros(star.shape)
        self.update_param['c_0'] =  numpy.zeros(input_state.shape)

        ## temperary values to facilitate calculation of dL/dtheta
        # dh_(t-1)/dp
        self.dht_dp = OrderedDict()
        self.dht_dp['lstm_W'] = numpy.zeros((input_state.size, update_param['lstm_W'].shape[0], update_param['lstm_W'].shape[1] ), dtype = config.floatX)
        self.dht_dp['lstm_U'] = numpy.zeros((input_state.size, update_param['lstm_U'].shape[0], update_param['lstm_U'].shape[1] ), dtype = config.floatX)
        self.dht_dp['lstm_b'] = numpy.zeros((input_state.size, decoder_model.tparams['lstm_b'].get_value().size), dtype = config.floatX)
        self.dht_dp['star'] = numpy.zeros((input_state.size, input_state.size), dtype = config.floatX)
        self.dht_dp['c_0'] = numpy.zeros((input_state.size, input_state.size), dtype = config.floatX)

        # dc_(t-1)/dp
        self.dct_dp = OrderedDict()
        self.dct_dp['lstm_W'] = numpy.zeros((input_state.size, update_param['lstm_W'].shape[0], update_param['lstm_W'].shape[1] ), dtype = config.floatX)
        self.dct_dp['lstm_U'] = numpy.zeros((input_state.size, update_param['lstm_U'].shape[0], update_param['lstm_U'].shape[1] ), dtype = config.floatX)
        self.dct_dp['lstm_b'] = numpy.zeros((input_state.size, decoder_model.tparams['lstm_b'].get_value().size), dtype = config.floatX)
        self.dct_dp['star'] = numpy.zeros((input_state.size, input_state.size), dtype = config.floatX)
        self.dct_dp['c_0'] = numpy.zeros((input_state.size, input_state.size), dtype = config.floatX)

        # function generation
        self.get_gradient_func()

    def get_gradient_func(self):
        """
        calculates gradient of projection(h_t) wrt to input cell state(c) and input vector(x) 
        """
        # to get dh_t/dc_(t-1)
        self.g_input_state_dh = theano.gradient.jacobian(self.proj.flatten(),self.input_state)
        # to get dh_t/dx_t
        self.g_input_dh = theano.gradient.jacobian(self.proj.flatten(),self.x)

        cell_state = self.complete_output[1][-1][-1]
        # to get dc_t/dc_(t-1)
        self.g_input_state_dc = theano.gradient.jacobian(cell_state.flatten(),self.input_state)
        # to get dc_t/dx_t
        self.g_input_dc = theano.gradient.jacobian(cell_state.flatten(),self.x)

        #dh/dW
        self.g_dh_lstm_W = theano.gradient.jacobian(self.proj.flatten(),self.tparams['lstm_W'])
        #dh/dU
        self.g_dh_lstm_U = theano.gradient.jacobian(self.proj.flatten(),self.tparams['lstm_U'])
        #dh/db
        self.g_dh_lstm_b = theano.gradient.jacobian(self.proj.flatten(),self.tparams['lstm_b'])

        #dc/dW
        self.g_dc_lstm_W = theano.gradient.jacobian(cell_state.flatten(),self.tparams['lstm_W'])
        #dc/dU
        self.g_dc_lstm_U = theano.gradient.jacobian(cell_state.flatten(),self.tparams['lstm_U'])
        #dc/db
        self.g_dc_lstm_b = theano.gradient.jacobian(cell_state.flatten(),self.tparams['lstm_b'])

        # for output state
        self.f_input_state_dh = theano.function([self.x, self.input_state], self.g_input_state_dh, name='f_grad_input_state_dh')
        self.f_input_dh = theano.function([self.x, self.input_state], self.g_input_dh, name='f_grad_input_dh')
        
        self.f_dh_dlstm_W = theano.function([self.x, self.input_state], self.g_dh_lstm_W, name='f_dh_dlstm_W')
        self.f_dh_dlstm_U = theano.function([self.x, self.input_state], self.g_dh_lstm_U, name='f_dh_dlstm_U')
        self.f_dh_dlstm_b = theano.function([self.x, self.input_state], self.g_dh_lstm_b, name='f_dh_dlstm_b')
        
        # for cell state
        self.f_input_state_dc = theano.function([self.x, self.input_state], self.g_input_state_dc, name='f_grad_input_state_dc')
        self.f_input_dc = theano.function([self.x, self.input_state], self.g_input_dc, name='f_grad_input_dc')
        
        self.f_dc_dlstm_W = theano.function([self.x, self.input_state], self.g_dc_lstm_W, name='f_dc_dlstm_W')
        self.f_dc_dlstm_U = theano.function([self.x, self.input_state], self.g_dc_lstm_U, name='f_dc_dlstm_U')
        self.f_dc_dlstm_b = theano.function([self.x, self.input_state], self.g_dc_lstm_b, name='f_dc_dlstm_b')

    def get_update_step(self, i, dl_dh, dht_dp, dct_dp, update_param, vec, input_state, lr=0.1):
        
        print 'step:' + str(i)
        
        ##values calculation
        print 'calculating...'
        # for output state
        print 'part 1'
        dht_dct_1 = self.f_input_state_dh(vec,input_state).reshape(vec.size,input_state.size)
        dht_dht_1 = self.f_input_dh(vec,input_state).reshape(vec.size,input_state.size)
        dh_dlstm_W = self.f_dh_dlstm_W(vec, input_state).swapaxes(0,1)
        dh_dlstm_U = self.f_dh_dlstm_U(vec, input_state).swapaxes(0,1)
        dh_dlstm_b = self.f_dh_dlstm_b(vec, input_state)
        
        # for cell state
        print 'part 2'
        dct_dct_1 = self.f_input_state_dc(vec,input_state).reshape(vec.size,input_state.size)
        dct_dht_1 = self.f_input_dc(vec,input_state).reshape(vec.size,input_state.size)
        dc_dlstm_W = self.f_dc_dlstm_W(vec, input_state).swapaxes(0,1)
        dc_dlstm_U = self.f_dc_dlstm_U(vec, input_state).swapaxes(0,1)
        dc_dlstm_b = self.f_dc_dlstm_b(vec, input_state)
        
        ## cache values
        print 'caching values...'
        print 'part 1'
        self.dht_dp['lstm_W'] = dht_dht_1.dot(dht_dp['lstm_W']) + dht_dct_1.dot(dct_dp['lstm_W']) + dh_dlstm_W
        self.dht_dp['lstm_U'] = dht_dht_1.dot(dht_dp['lstm_U']) + dht_dct_1.dot(dct_dp['lstm_U']) + dh_dlstm_U
        self.dht_dp['lstm_b'] = dht_dht_1.dot(dht_dp['lstm_b']) + dht_dct_1.dot(dct_dp['lstm_b']) + dh_dlstm_b
        self.dht_dp['star'] = dht_dht_1.dot(dht_dp['star']) + dht_dct_1.dot(dct_dp['star']) 
        self.dht_dp['c_0'] = dht_dht_1.dot(dht_dp['c_0']) + dht_dct_1.dot(dct_dp['c_0']) 
        if i == 1:
            self.dht_dp['star'] += dht_dht_1
            self.dht_dp['c_0'] += dht_dct_1
            
        print 'part 2'
        self.dct_dp['lstm_W'] = dct_dct_1.dot(dct_dp['lstm_W']) + dct_dht_1.dot(dht_dp['lstm_W']) + dc_dlstm_W
        self.dct_dp['lstm_U'] = dct_dct_1.dot(dct_dp['lstm_U']) + dct_dht_1.dot(dht_dp['lstm_U']) + dc_dlstm_U
        self.dct_dp['lstm_b'] = dct_dct_1.dot(dct_dp['lstm_b']) + dct_dht_1.dot(dht_dp['lstm_b']) + dc_dlstm_b
        self.dct_dp['star'] = dct_dct_1.dot(dct_dp['star']) + dct_dht_1.dot(dht_dp['star']) 
        self.dct_dp['c_0'] = dct_dct_1.dot(dct_dp['c_0']) + dct_dht_1.dot(dht_dp['c_0']) 
        if i == 1:
            self.dct_dp['star'] += dct_dct_1
            self.dct_dp['c_0'] += dct_dht_1

        # updates
        print 'storing updates...'
        self.update_param['lstm_W'] +=  -lr*dl_dh.dot(dht_dp['lstm_W'])[0]
        self.update_param['lstm_U'] =  -lr*dl_dh.dot(dht_dp['lstm_U'])[0]  
        self.update_param['lstm_b'] =  -lr*dl_dh.dot(dht_dp['lstm_b'])
        self.update_param['star'] =  -lr*dl_dh.dot(dht_dp['star'])
        self.update_param['c_0'] =  -lr*dl_dh.dot(dht_dp['c_0'])

        print 'update done'
        # return dht_dp, dct_dp, update_param

    def get_update(self, star, ground_truth, decode_model, env, input_state=numpy.array([])):
        """
        update parameters from ground truth
        """
        if input_state.size == 0:
            input_state = numpy.random.randn((star.shape[0],decode_model.model_options['cell_state_dim'])).astype(config.floatX)
        
        x_ = star
        c_ = input_state
        get_action_model = decode_action(num_predicates=4, num_object_categories=76, num_state_space=34, dim_encoding=250)
            
        for i in range(len(ground_truth)):
            encoding = decode_model.get_encoding_from_vec(x_, c_)
            loss, dl_dh = get_action_model.get_loss_and_backprop(encoding,env, ground_truth[i][0], ground_truth[i][1])
            dht_dp, dct_dp, update_param = decode_model.get_update_step(i+1, dl_dh,  dht_dp, dct_dp, update_param , x_, c_, lr=0.1)
            
            # new input and state variable
            c_ = decode_model.get_input_state_from_vec(x_, c_).T
            x_ = numpy.copy(encoding.T)

        # updating parameters
        decode_model.tparams['lstm_W'] += self.update_param['lstm_W']
        decode_model.tparams['lstm_U'] += self.update_param['lstm_U']
        decode_model.tparams['lstm_b'] += self.update_param['lstm_b']
        star += update_param['star']
        return update_param['c_0'], star

class Encoder_update(object):
    
    def __init__(self, encoder_model):
        """
        initializing function to calculate the update
        """ 
        # gradients
        g_dh_dlstm_W = theano.gradient.jacobian(encoder_model.proj.flatten(), wrt=encoder_model.tparams['lstm_W'])
        g_dh_dlstm_U = theano.gradient.jacobian(encoder_model.proj.flatten(), wrt=encoder_model.tparams['lstm_U'])
        g_dh_dlstm_b = theano.gradient.jacobian(encoder_model.proj.flatten(), wrt=encoder_model.tparams['lstm_b'])
        
        # function
        self.f_dh_dlstm_W = theano.function([encoder_model.x, encoder_model.input_state], g_dh_dlstm_W, name='f_dh_dlstm_W')
        self.f_dh_dlstm_U = theano.function([encoder_model.x, encoder_model.input_state], g_dh_dlstm_U, name='f_dh_dlstm_U')
        self.f_dh_dlstm_b = theano.function([encoder_model.x, encoder_model.input_state], g_dh_dlstm_b, name='f_dh_dlstm_b')
    
    def get_update(self, dl_dh, string, encoder_model, lr=0.1):
        """
        for calculating the update
        """
        wordEmb = encoder_model.get_wordVec(string)
        vec, mask, y = prepare_data(wordEmb, 1)
        input_state = numpy.zeros((vec.shape[1],encoder_model.model_options['dim_proj'])).astype(config.floatX)
        # calculate values
        # print 'calculate values'
        dh_dlstm_W = self.f_dh_dlstm_W(vec, input_state).swapaxes(0,1)
        dh_dlstm_U = self.f_dh_dlstm_U(vec, input_state).swapaxes(0,1)
        dh_dlstm_b = self.f_dh_dlstm_b(vec, input_state)

        # updating parameters
        encoder_model.tparams['lstm_W'].set_value(encoder_model.tparams['lstm_W'].get_value() -lr*dl_dh.dot(dh_dlstm_W)[0]) 
        encoder_model.tparams['lstm_U'].set_value(encoder_model.tparams['lstm_U'].get_value() -lr*dl_dh.dot(dh_dlstm_U)[0]) 
        encoder_model.tparams['lstm_b'].set_value(encoder_model.tparams['lstm_b'].get_value() + (-lr*dl_dh.dot(dh_dlstm_b)).flatten()) 
