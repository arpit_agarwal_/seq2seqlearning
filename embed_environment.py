import numpy
dense_dim = 50

class embed_environment:
    
    def __init__(self, obj_dim=50):
        self.obj_dim = obj_dim
        self.objs = None
        
    def calculate_environment_embedding(self, objs):
        """ Given dense embedding of objects returns embedding of the
            environment
              param: objs is a list of embedding of objects 
                     (every vector is a column vector)
              Embedding given by: avg(x)
        """
        self.objs = objs
        embedding = numpy.zeros(self.obj_dim)
          
        for obj in objs:
            embedding = embedding + obj
        embedding = embedding/float(len(objs))
        return embedding

    def set_input(self, objs):
        """ changes the input which is used for backprop """
        self.objs = objs

    def backprop(self, error):
        """ Returns errors for individual objects
          y = 1/n * \sum_i obj_i
          dl/dobj_i = dl/dy * dy/dobj_i = 1/n dl/dy
        """
        grads = []
        n = len(self.objs)

        for obj in self.objs:
            grad = numpy.copy(obj)
            grad = grad/float(n)
            grads.append(grad)
        return grads    