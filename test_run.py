from LSTM import LSTM
import numpy
from Environment import Environment
from Update import Encoder_update
import time
from decode_action import decode_action
import pickle 
from embed_environment import dense_dim
from theano import config
# random initialization
SEED = 123
numpy.random.seed(SEED)

if __name__ == '__main__':

    reload_params = False
    save_params = False
    ## loading encoder, environment, decoder and get actions model
    # encoder model
    encode_model = LSTM()
    env_model = Environment()
    
    # decoder model and star parameters
    dim = encode_model.model_options['cell_state_dim'] + dense_dim
    decode_model = LSTM(dict_f = None, dim_proj=dim, cell_state_dim = dim )
    decode_model.tparams['Wemb'] = None
    star = numpy.random.randn(1,dim)#.astype(config.floatX)

    # get action model 
    get_action_model = decode_action(num_predicates=5, num_object_categories=76, num_state_space=34, dim_encoding=250)
    
    # update model
    decode_model.get_gradient_func()
    encoder_update_model = Encoder_update(encode_model)   

    # load the dataset 
    dataset = pickle.load(open('dataset/groundTruth.dict','r'))

    if reload_params:
        a = numpy.load('params-2-4.npz')

        encode_model.tparams['lstm_W'].set_value(a['encode_W'])
        encode_model.tparams['lstm_U'].set_value(a['encode_U'])
        encode_model.tparams['lstm_b'].set_value(a['encode_b'])

        decode_model.tparams['lstm_W'].set_value(a['decode_W'])
        decode_model.tparams['lstm_U'].set_value(a['decode_U'])
        decode_model.tparams['lstm_b'].set_value(a['decode_b'])

        star = a['star']

    for j in range(1,2):
        print 'point Num:' + str(j)
        t0 = time.time()
        sentence = dataset[j]['text']
        ground_truth = numpy.array(dataset[j]['action'])
        env_num = dataset[j]['env']

        # load environment
        env_name = 'Environments/livingRoom'+env_num+'.xml'
        env_model.load_environment(env_name)
        # get environment  
        env_vec = env_model.get_env_embedding()

        for i in range(1):

            print 'epoch num:' + str(i)
            # evaluating the sentence encoding
            sen_encoding = encode_model.get_encoding_from_string(string=sentence)
            input_state = numpy.concatenate((sen_encoding, env_vec), axis=0)
            input_state.shape = (input_state.shape[0],1)
            input_state = input_state.transpose()
            print 'done'
            
            
            # update
            # decoder params update
            dl_dc_0, star = decode_model.get_update(star, ground_truth, env = env_model,  get_action_model= get_action_model, input_state=input_state)

            # encoder params update
            dl_dh = dl_dc_0[0,:sen_encoding.size]
            encoder_update_model.get_update(dl_dh, sentence, encode_model) 



            # #save parameters
            if save_params:
                filename = 'weights/params-'+ str(i) + '-'+str(j)+'.npz'
                f = open(filename,'w')
                numpy.savez(f,
                         encode_W=encode_model.tparams['lstm_W'].get_value(),
                         encode_U=encode_model.tparams['lstm_U'].get_value(),
                         encode_b=encode_model.tparams['lstm_b'].get_value(),
                         decode_W=decode_model.tparams['lstm_W'].get_value(),
                         decode_U=decode_model.tparams['lstm_U'].get_value(),
                         decode_b=decode_model.tparams['lstm_b'].get_value(),
                         star = star,
                         env_W = env_model.W_sparse_to_dense,

                         )

        print 'time per ex/instruction:',
        print (time.time()-t0)/len(ground_truth)

  # star = update_model.get_update(encode_model, decode_model, env_model, star, ground_truth, input_state)


    # to get actions from decoder
    # action = decode_model.run_until_no_action(star, input_state=input_state, env=env_model, maxItr=2)
    # print action
    
