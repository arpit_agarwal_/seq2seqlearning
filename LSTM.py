"""
The LSTM class accepts 2 inputs:
1) natural language sentences
2) vectors
There are added control variables in the model as listed below:
1) number of class for the classifier for softmax layer (hU+b)
2) input for word embeding dictionary file
3) manual cell state(h) dimension = 200(different from input state)
4) input state initialization
"""

from collections import OrderedDict
import cPickle as pkl
import sys
import time

import numpy
import theano
from theano import config
import theano.tensor as tensor
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

import imdb
from optimization_func import sgd, adadelta, rmsprop
from utils import numpy_floatX, get_minibatches_idx, zipp, unzip, prepare_data
from decode_action import decode_action
import re
# Set the random number generators' seeds for consistency
SEED = 123
numpy.random.seed(SEED)

class LSTM(object):
    
    x = tensor.matrix('x', dtype=config.floatX)
    y = tensor.vector('y', dtype='int32')
    input_state = tensor.matrix(name='input_state', dtype=config.floatX)

    def __init__(self,
        dim_proj=200,  # word embeding dimension and LSTM number of hidden units.
        patience=10,  # Number of epoch to wait before early stop if no progress
        max_epochs=5000,  # The maximum number of epoch to run
        dispFreq=10,  # Display to stdout the training progress every N updates
        lrate=0.0001,  # Learning rate for sgd (not used for adadelta and rmsprop)
        n_words=10000,  # Vocabulary size
        optimizer=adadelta,  # sgd, adadelta and rmsprop available, sgd very hard to use, not recommanded (probably need momentum and decaying learning rate).
        saveto='lstm_model.npz',  # The best model will be saved there
        validFreq=370,  # Compute the validation error after this number of update.
        saveFreq=1110,  # Save the parameters after every saveFreq updates
        maxlen=100,  # Sequence longer then this get ignored
        batch_size=16,  # The batch size during training.
        valid_batch_size=64,  # The batch size used for validation/test set.
        
        # Parameter for extra option
        noise_std=0.,
        use_dropout=True,  # if False slightly faster, but worst test error
                           # This frequently need a bigger model.
        reload_model=None,  # Path to a saved model we want to start from.
        test_size=-1,  # If >0, we keep only this number of test example.
        num_class = 2, #number of class for the classifier for softmax layer (hU+b)
        dict_f='vocab_word2vec.txt',
        cell_state_dim = 200
    ):

        # Model options
        self.model_options = locals().copy()
        del self.model_options['self']#, self.model_options['num_class']
        
        # to reduce test_size if required
        
        self.params = self.init_params(num_class,filename=dict_f)

        # check for reloading model params
        if reload_model:
            self.load_params(reload_model, self.params)

        # shared stuff
        self.init_tparams()
        self.build_model()

        # self.f_cost = theano.function([self.x, self.y, self.input_state], self.cost, name='f_cost')


    def _p(self, pp, name):
        return '%s_%s' % (pp, name)

    def get_learning_func(self):    
        """create learning functions 
        """
        self.grads = tensor.grad(self.cost, wrt=self.tparams.values())
        self.f_grad = theano.function([self.x, self.y, self.input_state], self.grads, name='f_grad')

        self.lr = tensor.scalar(name='lr')
        optimizer = self.model_options['optimizer']
        self.f_grad_shared, self.f_update = optimizer(self.lr, self.tparams, self.grads,
                                        self.x, self.y, self.cost, self.input_state)

    def lstm_layer(self, state_below):
        """
        core lstm functions(simplest model for now)

        """
        tparams = self.tparams
        options = self.model_options
        prefix='lstm'
        
        
        if state_below.ndim == 3:
            nsteps = state_below.shape[0]
            n_samples = state_below.shape[1]
        else:
            nsteps = 1
            n_samples = 1

        def _slice(_x, n, dim):
            if _x.ndim == 3:
                return _x[:, :, n * dim:(n + 1) * dim]
            return _x[:, n * dim:(n + 1) * dim]

        def _step( x_, h_, c_):
            preact = tensor.dot(h_, tparams[self._p(prefix, 'U')])
            preact += x_

            i = tensor.nnet.sigmoid(_slice(preact, 0, options['cell_state_dim']))
            f = tensor.nnet.sigmoid(_slice(preact, 1, options['cell_state_dim']))
            o = tensor.nnet.sigmoid(_slice(preact, 2, options['cell_state_dim']))
            c = tensor.tanh(_slice(preact, 3, options['cell_state_dim']))

            # elementwise multiplication
            c = f * c_ + i * c
            h = o * tensor.tanh(c)
            
            return h, c

        state_below = (tensor.dot(state_below, tparams[self._p(prefix, 'W')]) +
                       tparams[self._p(prefix, 'b')])

        dim_proj = options['dim_proj']
        cell_state_dim = options['cell_state_dim']
        ## rval: output, updates: cell state
        rval, updates = theano.scan(_step,
                                    sequences=[state_below],
                                    outputs_info=[
                                                    # tensor.alloc(numpy_floatX(0.),
                                                    #            n_samples,
                                                               # cell_state_dim),
                                                  self.input_state,
                                                  tensor.alloc(numpy_floatX(0.),
                                                               n_samples,
                                                               cell_state_dim)
                                                    ],
                                    name=self._p(prefix, '_layers'),
                                    n_steps=nsteps)
        
        self.complete_output = rval
        
        return rval[0][-1]  #rval[0]:tensor of h_ts, rval[0][-1][-1]: last h_t

    def dropout_layer(self, state_before, use_noise, trng):
        """
        to perform random switch operation
        """
        proj = tensor.switch(use_noise,
                             (state_before *
                              trng.binomial(state_before.shape,
                                            p=0.5, n=1,
                                            dtype=state_before.dtype)),
                             state_before * 0.5)
        return proj

    def build_model(self):
        """
        model with input settings and function for probability generation
        """
        trng = RandomStreams(SEED)
        tparams = self.tparams
        options = self.model_options
        # Used for dropout.
        self.use_noise = theano.shared(numpy_floatX(0.))

        x = self.x
        y = self.y

        if self.params['Wemb'].size:
            # print tparams['Wemb'].get_value()
            n_timesteps = x.shape[0]
            n_samples = x.shape[1]
            emb = tparams['Wemb'][x.astype('int32').flatten()].reshape([n_timesteps,
                                                    n_samples,
                                                    options['dim_proj']])
        else:
            n_timesteps = 1
            emb = x
            n_samples = 1
        
        ## lstm layer func output(only last)
        proj = self.lstm_layer(emb)
        self.proj = proj[-1]

        if options['use_dropout']:
            proj = self.dropout_layer(proj, self.use_noise, trng)

        pred = tensor.nnet.softmax(tensor.dot(proj, tparams['U']) + tparams['b'])
        # pred = tensor.dot(proj, tparams['U']) + tparams['b']
        self.pred = pred
        
        #f_pred = theano.function([x, self.input_state], pred, name='f_pred') ## max probability func
        #self.f_pred = f_pred

        off = 1e-8
        if pred.dtype == 'float16':
            off = 1e-6

        cost = -tensor.log(pred[tensor.arange(n_samples), y] + off).mean()
        # cost = -tensor.log(pred + off)
        self.cost = cost

        # return use_noise, f_pred, cost, proj
    
    def load_params(self, path, params):
        """load previously saved parameters from 
            path
        """
        pp = numpy.load(path)
        for kk, vv in params.iteritems():
            if kk not in pp:
                raise Warning('%s is not in the archive' % kk)
            params[kk] = pp[kk]
        
        return params

    def init_tparams(self):
        """creates shared params from normal params 
            to be used in symbolic expressions
        """
        self.tparams = OrderedDict()
        for kk, pp in self.params.iteritems():
            self.tparams[kk] = theano.shared(self.params[kk], name=kk)
        
    
    def ortho_weight(self, ndim, mdim=0):
        """initializes parameters with orthogonal random vectors
        """
        # print ndim, mdim
        if mdim == 0:
            mdim = ndim
        W = numpy.random.randn(ndim, mdim)
        u, s, v = numpy.linalg.svd(W, full_matrices=False)
        # print v.shape
        if ndim >= mdim:
            return u.astype(config.floatX)
        else:
            return v.astype(config.floatX)

    def param_init_lstm(self, params, prefix='lstm'):
        """Init the LSTM parameter:
        """
        options = self.model_options
        W = numpy.concatenate([self.ortho_weight(options['dim_proj'], options['cell_state_dim']),
                               self.ortho_weight(options['dim_proj'], options['cell_state_dim']),
                               self.ortho_weight(options['dim_proj'], options['cell_state_dim']),
                               self.ortho_weight(options['dim_proj'], options['cell_state_dim'])], axis=1)
        params[self._p(prefix, 'W')] = W
        # print 'Wdim:',
        # print W.shape
        U = numpy.concatenate([self.ortho_weight(options['cell_state_dim']),
                               self.ortho_weight(options['cell_state_dim']),
                               self.ortho_weight(options['cell_state_dim']),
                               self.ortho_weight(options['cell_state_dim'])], axis=1)
        params[self._p(prefix, 'U')] = U
        # print 'Udim:',
        # print U.shape
        b = numpy.zeros((4 * options['cell_state_dim'],))
        params[self._p(prefix, 'b')] = b.astype(config.floatX)

        return params

    def load_wordEmbedding(self, filename=None, dictFile='cornell.dict.pkl'):
        """loads word vector for all the words in the dict
        """
        options = self.model_options
        randn = numpy.random.rand(options['n_words'],
                                  options['dim_proj'])
        emb = (0.01 * randn).astype(config.floatX)
        word = pkl.load( open( dictFile, "r" ) )

        if filename:
            with open(filename, 'r') as f:
                for line in f:
                    tmp = line.split(':')
                    if tmp[0] in word.keys() and word[tmp[0]] < emb.shape[0]:
                        temp = tmp[1].split(',')
                        temp = [float(i) for i in temp]
                        emb[word[tmp[0]]] = temp 
        else:
            emb = numpy.array([])
        return emb                   

    def init_params(self,num_class, filename='vocab_word2vec.txt'):
        """
        Global (not LSTM) parameter. For the embeding and the classifier.
        """
        options = self.model_options
        params = OrderedDict()
        # embedding
        params['Wemb'] = self.load_wordEmbedding(filename)
        ## lstm init params func called
        params = self.param_init_lstm(params=params)
        # classifier
        params['U'] = 0.01 * numpy.random.randn(options['cell_state_dim'],
                                                num_class).astype(config.floatX)
        params['b'] = numpy.zeros((num_class,)).astype(config.floatX)

        return params

    
    #def pred_error(self, data, iterator, verbose=False):
        """
        Just compute the error
        f_pred: Theano fct computing the prediction
        """
     #   valid_err = 0
      #  for _, valid_index in iterator:
       #     x, mask, y = prepare_data([data[0][t] for t in valid_index],
        #                              numpy.array(data[1])[valid_index],
         #                             maxlen=None)
          #  preds = self.f_pred(x,y)
           # targets = numpy.array(data[1])[valid_index]
           # valid_err += (preds == targets).sum()
       # valid_err = 1. - numpy_floatX(valid_err) / len(data[0])

        #return valid_err

    def run_training(self
        , train, valid, test
        ,valid_batch_size=64  # The batch size used for validation/test set.
        ,batch_size=16  # The batch size during training.
        ,max_epochs=1
        ,test_size=10,
        
    ):
        """
        training the lstm
        """
        self.max_epochs = max_epochs
        self.test_size = test_size

        if test_size > 0:
            idx = numpy.arange(len(test[0]))
            numpy.random.shuffle(idx)
            idx = idx[:test_size]
            test = ([test[0][n] for n in idx], [test[1][n] for n in idx])

        # self.model_options['ydim'] = numpy.max(train[1]) + 1

        print 'Optimization'

        kf_valid = get_minibatches_idx(len(valid[0]), valid_batch_size)
        kf_test = get_minibatches_idx(len(test[0]), valid_batch_size)

        print "%d train examples" % len(train[0])
        print "%d valid examples" % len(valid[0])
        print "%d test examples" % len(test[0])

        history_errs = []
        best_p = None
        bad_count = 0
        validFreq = self.model_options['validFreq']
        saveFreq = self.model_options['saveFreq']
        saveto = self.model_options['saveto']
        use_noise = self.use_noise
        if validFreq == -1:
            validFreq = len(train[0]) / batch_size
        if saveFreq == -1:
            saveFreq = len(train[0]) / batch_size

        uidx = 0  # the number of update done
        estop = False  # early stop
        start_time = time.time()
        try:
            for eidx in xrange(self.max_epochs):
                n_samples = 0

                # Get new shuffled index for the training set.
                kf = get_minibatches_idx(len(train[0]), batch_size, shuffle=True)

                for _, train_index in kf:
                    uidx += 1
                    use_noise.set_value(1.)

                    # Select the random examples for this minibatch
                    y = [train[1][t] for t in train_index]
                    x = [train[0][t]for t in train_index]

                    # Get the data in numpy.ndarray format
                    # This swap the axis!
                    # Return something of shape (minibatch maxlen, n samples)
                    x, mask, y = prepare_data(x, y)
                    # print x.shape, x[:][0]
                    n_samples += x.shape[1]
                    # print x, x.shape
                    input_state = numpy.zeros((x.shape[1],self.model_options['dim_proj']))

                    cost = self.f_grad_shared(x, y, input_state)
                    self.f_update(self.model_options['lrate'])

                    if numpy.isnan(cost) or numpy.isinf(cost):
                        print 'bad cost detected: ', cost
                        return 1., 1., 1.

                    if numpy.mod(uidx, self.model_options['dispFreq']) == 0:
                        print 'Epoch ', eidx, 'Update ', uidx, 'Cost ', cost

                    if saveto and numpy.mod(uidx, saveFreq) == 0:
                        print 'Saving...',

                        if best_p is not None:
                            self.params = best_p
                        else:
                            self.params = unzip(self.tparams)
                        numpy.savez(saveto, history_errs=history_errs, **params)
                        pkl.dump(self.model_options, open('%s.pkl' % saveto, 'wb'), -1)
                        print 'Done'

                    if numpy.mod(uidx, validFreq) == 0:
                        use_noise.set_value(0.)
                        train_err = self.pred_error(train, kf)
                        valid_err = self.pred_error(valid,
                                               kf_valid)
                        test_err = self.pred_error(test, kf_test)

                        history_errs.append([valid_err, test_err])

                        if (best_p is None or
                            valid_err <= numpy.array(history_errs)[:,
                                                                   0].min()):

                            best_p = unzip(tparams)
                            bad_counter = 0

                        print ('Train ', train_err, 'Valid ', valid_err,
                               'Test ', test_err)

                        if (len(history_errs) > self.patience and
                            valid_err >= numpy.array(history_errs)[:-patience,
                                                                   0].min()):
                            bad_counter += 1
                            if bad_counter > self.patience:
                                print 'Early Stop!'
                                estop = True
                                break

                print 'Seen %d samples' % n_samples

                if estop:
                    break

        except KeyboardInterrupt:
            print "Training interupted"

        end_time = time.time()
        
        if best_p is not None:
            zipp(best_p, self.tparams)
        else:
            best_p = unzip(self.tparams)

        # final error calculation
        self.use_noise.set_value(0.)
        if saveto:
            numpy.savez(saveto, 
                        history_errs=history_errs, **best_p)
        print 'The code run for %d epochs, with %f sec/epochs' % (
            (eidx + 1), (end_time - start_time) / (1. * (eidx + 1)))
        print >> sys.stderr, ('Training took %.1fs' %
                              (end_time - start_time))
     
    def get_wordVec(self, string, dictFile = 'cornell.dict.pkl'):
        """
        get vector from NL input
        """
        wordMap = pkl.load( open( dictFile, "r" ))
        input_words = re.compile('\w+').findall(string)
        x = numpy.zeros((len(input_words), 1)).astype(config.floatX)
        k = 0
        for word in input_words:
            if word in wordMap.keys():
                tmp = wordMap[word]
                if tmp < self.model_options['n_words']:
                    x[k] = tmp
            k += 1
        
        return x    

    def get_encoding_from_string(self, string):
        """
        to get h_t from NL sentence as input
        """
        wordEmb = self.get_wordVec(string)
        # print wordEmb
        f_pred_embedding = theano.function([self.x, self.input_state], self.proj, name='f_pred_embedding')
        x, mask, y = prepare_data(wordEmb, 1)
        encoding = f_pred_embedding(x.astype(config.floatX),numpy.zeros((x.shape[1],self.model_options['dim_proj'])).astype(config.floatX))
        return encoding

    def get_encoding_from_vec(self,vec,input_state =numpy.array([])):
        """
        gives h_t from vector as input
        """
        f_pred_embedding = theano.function([self.x, self.input_state], self.proj, name='f_pred_embedding')
        if input_state.size == 0:
            input_state = numpy.zeros((vec.shape[0],self.model_options['cell_state_dim'])).astype(config.floatX)
        #print input_state.dtype        
        encoding = f_pred_embedding(vec.astype(config.floatX), input_state.astype(config.floatX))
        encoding.shape = (encoding.shape[0],1)
        return encoding

    def get_input_state_from_vec(self,vec,input_state =numpy.array([])):
        """
        gives h_t from vector as input
        """
        f_pred_input_state = theano.function([self.x, self.input_state], self.complete_output[1][-1][-1], name='f_pred_embedding')
        if input_state.size == 0:
            input_state = numpy.zeros((vec.shape[0],self.model_options['cell_state_dim'])).astype(config.floatX)
        
        input_state = f_pred_input_state(vec, input_state)
        input_state.shape = (input_state.shape[0],1)
        return input_state   

    def run_until_no_action(self,vec, env, input_state=numpy.array([]), maxItr=5):
        """
        gives actions from NL sentences(specific to the work)
        """
        IterNum = 0
        action_pred = 0 
        x_ = numpy.copy(vec)
        c_ = numpy.copy(input_state)
        actionModel = decode_action(num_predicates=4, num_object_categories=76, num_state_space=34, dim_encoding=250)
        
        action_list = []    
        while action_pred != -1 and IterNum < maxItr:
            print x_ , c_
            # print x_.shape, c_.shape
            IterNum += 1
            print IterNum
            encoding = self.get_encoding_from_vec(x_, c_)
            action, _ = actionModel.give_action(encoding,env, k=1)
            # print 'action index:' + str(action[0][0][0])
            action_pred = action[0][0][0]
            c_ = self.get_input_state_from_vec(x_, c_).T

            x_ = numpy.copy(encoding.astype(numpy.float64).T)
            
            action_list += action

        return action_list

    def get_gradient_func(self):
        """
        calculates gradient of projection(h_t) wrt to input cell state(c) and input vector(x) 
        """
        # to get dh_t/dc_(t-1)
        self.g_input_state_dh = theano.gradient.jacobian(self.proj.flatten(),self.input_state)
        # to get dh_t/dx_t
        self.g_input_dh = theano.gradient.jacobian(self.proj.flatten(),self.x)

        cell_state = self.complete_output[1][-1][-1]
        # to get dc_t/dc_(t-1)
        self.g_input_state_dc = theano.gradient.jacobian(cell_state.flatten(),self.input_state)
        # to get dc_t/dx_t
        self.g_input_dc = theano.gradient.jacobian(cell_state.flatten(),self.x)

        #dh/dW
        self.g_dh_lstm_W = theano.gradient.jacobian(self.proj.flatten(),self.tparams['lstm_W'])
        #dh/dU
        self.g_dh_lstm_U = theano.gradient.jacobian(self.proj.flatten(),self.tparams['lstm_U'])
        #dh/db
        self.g_dh_lstm_b = theano.gradient.jacobian(self.proj.flatten(),self.tparams['lstm_b'])

        #dc/dW
        self.g_dc_lstm_W = theano.gradient.jacobian(cell_state.flatten(),self.tparams['lstm_W'])
        #dc/dU
        self.g_dc_lstm_U = theano.gradient.jacobian(cell_state.flatten(),self.tparams['lstm_U'])
        #dc/db
        self.g_dc_lstm_b = theano.gradient.jacobian(cell_state.flatten(),self.tparams['lstm_b'])

        # for output state
        self.f_input_state_dh = theano.function([self.x, self.input_state], self.g_input_state_dh, name='f_grad_input_state_dh')
        self.f_input_dh = theano.function([self.x, self.input_state], self.g_input_dh, name='f_grad_input_dh')
        
        self.f_dh_dlstm_W = theano.function([self.x, self.input_state], self.g_dh_lstm_W, name='f_dh_dlstm_W')
        self.f_dh_dlstm_U = theano.function([self.x, self.input_state], self.g_dh_lstm_U, name='f_dh_dlstm_U')
        self.f_dh_dlstm_b = theano.function([self.x, self.input_state], self.g_dh_lstm_b, name='f_dh_dlstm_b')
        
        # for cell state
        self.f_input_state_dc = theano.function([self.x, self.input_state], self.g_input_state_dc, name='f_grad_input_state_dc')
        self.f_input_dc = theano.function([self.x, self.input_state], self.g_input_dc, name='f_grad_input_dc')
        
        self.f_dc_dlstm_W = theano.function([self.x, self.input_state], self.g_dc_lstm_W, name='f_dc_dlstm_W')
        self.f_dc_dlstm_U = theano.function([self.x, self.input_state], self.g_dc_lstm_U, name='f_dc_dlstm_U')
        self.f_dc_dlstm_b = theano.function([self.x, self.input_state], self.g_dc_lstm_b, name='f_dc_dlstm_b')

    def get_update(self, star, ground_truth, env, get_action_model, input_state=numpy.array([])):
        """
        update parameters from ground truth
        """
        ## init
        if input_state.size == 0:
            input_state = numpy.zeros((star.shape[0],self.model_options['cell_state_dim'])).astype(config.floatX)
        
        # parameters to be updated
        update_param = OrderedDict()
        update_param['lstm_W'] =  numpy.zeros(self.tparams['lstm_W'].get_value().shape)
        update_param['lstm_U'] =  numpy.zeros(self.tparams['lstm_U'].get_value().shape)
        update_param['lstm_b'] =  numpy.zeros(self.tparams['lstm_b'].get_value().shape)
        update_param['star'] =  numpy.zeros(star.shape)
        update_param['c_0'] =  numpy.zeros(input_state.shape)

        ## temperary values to facilitate calculation of dL/dtheta
        # dh_(t-1)/dp
        dht_dp = OrderedDict()
        dht_dp['lstm_W'] = numpy.zeros((input_state.size, update_param['lstm_W'].shape[0], update_param['lstm_W'].shape[1] ), dtype = config.floatX)
        dht_dp['lstm_U'] = numpy.zeros((input_state.size, update_param['lstm_U'].shape[0], update_param['lstm_U'].shape[1] ), dtype = config.floatX)
        dht_dp['lstm_b'] = numpy.zeros((input_state.size, self.tparams['lstm_b'].get_value().size), dtype = config.floatX)
        dht_dp['star'] = numpy.zeros((input_state.size, input_state.size), dtype = config.floatX)
        dht_dp['c_0'] = numpy.zeros((input_state.size, input_state.size), dtype = config.floatX)

        # dc_(t-1)/dp
        dct_dp = OrderedDict()
        dct_dp['lstm_W'] = numpy.zeros((input_state.size, update_param['lstm_W'].shape[0], update_param['lstm_W'].shape[1] ), dtype = config.floatX)
        dct_dp['lstm_U'] = numpy.zeros((input_state.size, update_param['lstm_U'].shape[0], update_param['lstm_U'].shape[1] ), dtype = config.floatX)
        dct_dp['lstm_b'] = numpy.zeros((input_state.size, self.tparams['lstm_b'].get_value().size), dtype = config.floatX)
        dct_dp['star'] = numpy.zeros((input_state.size, input_state.size), dtype = config.floatX)
        dct_dp['c_0'] = numpy.zeros((input_state.size, input_state.size), dtype = config.floatX)

        x_ = star
        c_ = input_state
            
        for i in range(len(ground_truth)):
            encoding = self.get_encoding_from_vec(x_, c_)
            loss, dl_dh = get_action_model.get_loss_and_backprop(encoding,env, ground_truth[i][0], ground_truth[i][1])
            dht_dp, dct_dp, update_param = self.get_update_step(i+1, dl_dh,  dht_dp, dct_dp, update_param , vec=x_, input_state = c_, lr=0.1)
	    print 'loss in instruction num ' + str(i) + ':' + str(loss)            
            # new input and state variable
            c_ = self.get_input_state_from_vec(x_, c_).T
            x_ = numpy.copy(encoding.astype(numpy.float64).T)

        # updating parameters
        self.tparams['lstm_W'].set_value(update_param['lstm_W']+self.tparams['lstm_W'].get_value())
        self.tparams['lstm_U'].set_value(update_param['lstm_U']+self.tparams['lstm_U'].get_value())
        self.tparams['lstm_b'].set_value((update_param['lstm_b']+self.tparams['lstm_b'].get_value()).flatten())
        star += update_param['star']
        return update_param['c_0'], star

    def get_update_step(self, i, dl_dh, dht_dp, dct_dp, update_param, vec, input_state, lr=0.1):
        
        #print 'step:' + str(i)

        # function generation
        # self.get_gradient_func()
        
        
        ##values calculation
        #print 'calculating...'
        # for output state
        #print 'part 1'
        dht_dct_1 = self.f_input_state_dh(vec,input_state).reshape(vec.size,input_state.size)
        dht_dht_1 = self.f_input_dh(vec,input_state).reshape(vec.size,input_state.size)
        dh_dlstm_W = self.f_dh_dlstm_W(vec, input_state).swapaxes(0,1)
        dh_dlstm_U = self.f_dh_dlstm_U(vec, input_state).swapaxes(0,1)
        dh_dlstm_b = self.f_dh_dlstm_b(vec, input_state)
        
        # for cell state
        #print 'part 2'
        dct_dct_1 = self.f_input_state_dc(vec,input_state).reshape(vec.size,input_state.size)
        dct_dht_1 = self.f_input_dc(vec,input_state).reshape(vec.size,input_state.size)
        dc_dlstm_W = self.f_dc_dlstm_W(vec, input_state).swapaxes(0,1)
        dc_dlstm_U = self.f_dc_dlstm_U(vec, input_state).swapaxes(0,1)
        dc_dlstm_b = self.f_dc_dlstm_b(vec, input_state)
        
        ## cache values
        #print 'caching values...'
        #print 'part 1'
        dht_dp['lstm_W'] = dht_dht_1.dot(dht_dp['lstm_W']) + dht_dct_1.dot(dct_dp['lstm_W']) + dh_dlstm_W
        dht_dp['lstm_U'] = dht_dht_1.dot(dht_dp['lstm_U']) + dht_dct_1.dot(dct_dp['lstm_U']) + dh_dlstm_U
        dht_dp['lstm_b'] = dht_dht_1.dot(dht_dp['lstm_b']) + dht_dct_1.dot(dct_dp['lstm_b']) + dh_dlstm_b
        dht_dp['star'] = dht_dht_1.dot(dht_dp['star']) + dht_dct_1.dot(dct_dp['star']) 
        dht_dp['c_0'] = dht_dht_1.dot(dht_dp['c_0']) + dht_dct_1.dot(dct_dp['c_0']) 
        if i == 1:
            dht_dp['star'] += dht_dht_1
            dht_dp['c_0'] += dht_dct_1
            
        #print 'part 2'
        dct_dp['lstm_W'] = dct_dct_1.dot(dct_dp['lstm_W']) + dct_dht_1.dot(dht_dp['lstm_W']) + dc_dlstm_W
        dct_dp['lstm_U'] = dct_dct_1.dot(dct_dp['lstm_U']) + dct_dht_1.dot(dht_dp['lstm_U']) + dc_dlstm_U
        dct_dp['lstm_b'] = dct_dct_1.dot(dct_dp['lstm_b']) + dct_dht_1.dot(dht_dp['lstm_b']) + dc_dlstm_b
        dct_dp['star'] = dct_dct_1.dot(dct_dp['star']) + dct_dht_1.dot(dht_dp['star']) 
        dct_dp['c_0'] = dct_dct_1.dot(dct_dp['c_0']) + dct_dht_1.dot(dht_dp['c_0']) 
        if i == 1:
            dct_dp['star'] += dct_dct_1
            dct_dp['c_0'] += dct_dht_1

        # updates
        #print 'storing updates...'
        # print update_param
        update_param['lstm_W'] +=  -lr*dl_dh.dot(dht_dp['lstm_W'])[0]
        update_param['lstm_U'] +=  -lr*dl_dh.dot(dht_dp['lstm_U'])[0]  
        update_param['lstm_b'] +=  (-lr*dl_dh.dot(dht_dp['lstm_b'])).flatten()
        update_param['star'] +=  -lr*dl_dh.dot(dht_dp['star'])
        update_param['c_0'] +=  -lr*dl_dh.dot(dht_dp['c_0'])
        # print update_param
        #print 'update done'
        return dht_dp, dct_dp, update_param

if __name__ == '__main__':
    
    ## TEST CASES

    # TEST 1
    # reload the model
    myModel = LSTM(reload_model='lstm_model.npz')
    print myModel.get_learning_func()
    vec = numpy.zeros((1,200)).astype(config.floatX)
    y = numpy.zeros(1).astype('int32')
    print myModel.f_grad(vec,y,vec) # x, y , input state
    
    # TEST 2
    "train the model"
    # print 'Loading data'
    # train, valid, test = imdb.load_data(n_words=10000, valid_portion=0.05,
    #                                maxlen=100)
    # myModel = LSTM(cell_state_dim = 200)
    # myModel.get_learning_func()
    # myModel.run_training(train, valid, test,
    #     max_epochs=1,
    #     test_size=10,
    # )

    # INPUT TO ABOVE TEST CASES
    # sentence = 'I love ball and crickets'
    # encoding_from_string = myModel.get_encoding_from_string(string=sentence)
    # print encoding_from_string.shape, encoding_from_string

    # TEST 4
    # myModel = LSTM(
    #                 # reload_model='lstm_model.npz', 
                    # dict_f = None, dim_proj = 200)
    # myModel.tparams['Wemb'] = None
    # encoding_from_vec = myModel.get_encoding_from_vec(numpy.zeros((1,200)).astype(config.floatX))
    # print encoding_from_vec.shape, encoding_from_vec


    
