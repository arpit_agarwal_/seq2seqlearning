import numpy
import math
from theano import tensor as T
import theano

from Environment import Environment

# random initialization
SEED = 123
numpy.random.seed(SEED)

#This class defines functionality for decoding a vector
#into an actual action along with returning probability
#of action. Currently handling only predicates with a single
#argument of type physical object.

class decode_action:

    def __init__(self, num_predicates, num_object_categories, num_state_space, dim_encoding):
        """ num_predicates, object_space and state_space are lists of all
            predicates, object categories and states. dim_encoding represents
            the dimensionality of the action encoding
        """
        self.num_predicates = num_predicates
        self.num_object_categories = num_object_categories
        self.num_state_space = num_state_space
        self.dim = dim_encoding

        #softmax parameter for predicate
        self.W_pred = numpy.random.randn(num_predicates, dim_encoding)
        self.b_pred = numpy.random.randn(num_predicates, 1)

        #softmax parameter for object categories
        self.W_obj = numpy.random.randn(num_object_categories, dim_encoding)
        self.b_obj = numpy.random.randn(num_object_categories, 1)

        #parameters for state_space encoding
        self.W_state = numpy.random.randn(num_state_space, dim_encoding)
        self.b_state = numpy.random.randn(num_state_space, 1)

        # function
        x,y,b = T.dvectors('x','y','b')
        W = T.dmatrix('W')
        y = T.nnet.softmax(T.dot(W,x) + b)
        self.softmax_func = theano.function([x, W, b], y, name='softmax_func')

    def softmax(self, encoding, W_in, b_in):
        
        return self.softmax_func(encoding.flatten(), W_in, b_in.flatten()).T

    def sigmoid(self, vec):
        """ performs component wise sigmoid on a column vector"""
        assert vec.shape[1] is 1 #assert that its a column vector
        for i in range(0, vec.shape[0]):
            val = vec[i, 0]
            vec[i, 0] = 1/(1 + math.exp(-val))  
        return vec

    def give_action(self, encoding, env, k = -1):
        """given encoding of an action, the environment and a value k, this returns
           the top k action with highest probability given the encoding. Also returns partition
           function that is useful for learning. If k = -1
           then returns the entire list. Value of k is useful for performing beam search.
           The probability of an action is given by: 
           p(action = (f obj)) = p(f)p(obj) and 
           p(obj) = p(categ)p(state)
         
           where 
               p(f) = softmax(encoding, W, b)
               p(categ) = softmax(encoding, W, b)
               p(state) = product_j sigmoid( W[encoding] + b)_j
           This is a probability distribution over all possible actions
           and predicts action or objects which are not present in an environment.
           Therefore, given an environment e; we define 
           p(action | e ) as proportional to p(action) with entire probability
           given as p(action |e ) =
               p(action)1{action is valid or not} / \sum_{action' that are valid in e} p(action')
        """

        #actions and their probability
        action_and_prob = []
        Z = 0.00000001 #partition function

        #iterate over all possible actions

        #softmax over predicate and object categories
        pred_probabilities = self.softmax(encoding, self.W_pred, self.b_pred)
        obj_probabilites = self.softmax(encoding, self.W_obj, self.b_obj)
        #sigmoid over state space
        state_probabilites = self.sigmoid(self.W_state.dot(encoding)   + self.b_state)
        # print pred_probabilities.shape, obj_probabilites.shape
        #print len(env.objList)
	for i in range(0, self.num_predicates): #actions in the environment         
            prob_pred = pred_probabilities[i]

            for obj_index, obj in enumerate(env.objList):   #objects in the environment
        
                #filter actions that are invalid in the environment

                #category index of this object
                obj_vec = obj.obj_vec_sparse

                categ_index = -1
                try:
                    # categ_index = obj[0:self.num_object_categories].index(1)
                    tmp = numpy.nonzero(obj_vec[0:self.num_object_categories])[0]
                    if tmp.size:
                        categ_index = tmp[0]
                    else: 
                        categ_index = 0 
                except ValueError:
                    raise AssertionError("Object vector must a one corresponding to some category")

                prob_obj = obj_probabilites[categ_index]
                prob_state = 1.0
                for j in range(0, self.num_state_space):
                    if obj_vec[self.num_object_categories + j] == 1:
                        prob_state = prob_state * state_probabilites[j] 
                    else:
                        prob_state = prob_state * (1 - state_probabilites[j])
            
                #find unnormalized probability of this action
                unnorm_prob = prob_pred * prob_obj * prob_state
                Z = Z + unnorm_prob
                action_and_prob.append(((i, obj_index), unnorm_prob[0]))
        
        Z = Z[0]    
        #normalize the probability
        for i in range(0, len(action_and_prob)):
            action_and_prob[i] = (action_and_prob[i][0], action_and_prob[i][1]/Z)
            
                
        #sort the list action_and_prob
        action_and_prob.sort(key=lambda x: x[1])

        #return the top k
        if k == -1:
            return action_and_prob,Z
        else: 
            return action_and_prob[0:k],Z

    def get_loss(self, encoding, env, pred_ix, obj_ix):
        """
        gives loss from encoding and environment
        """
        results_and_Z = self.give_action(encoding, env, -1)
        results = results_and_Z[0]
        # print results 
        Z = results_and_Z[1]
        
        gtruth_ix = -1 
        for i in range(0, len(results)):
            if results[i][0][0] == pred_ix and results[i][0][1] == obj_ix:
                gtruth_ix = i
                break
        assert not (gtruth_ix == -1)

        #loss is negative-loss likelihood given by -logp(ground-truth)
        # print results[gtruth_ix][1]
        loss = -math.log(results[gtruth_ix][1])
        print "loss: "+str(loss)
        return loss


    def get_loss_and_backprop(self, encoding, env, pred_ix, obj_ix):
        """This function computes loss given an encoding, environment, 
           ground truth predicate index and the object index, and updates
           the parameter defined in this file and returns dloss/dencoding.
        """
        results_and_Z = self.give_action(encoding, env, -1)
        results = results_and_Z[0]
        # print results 
        Z = results_and_Z[1]
	#print '\n'        
        gtruth_ix = -1 
        for i in range(0, len(results)):
            #print results[i][0][0], results[i][0][1] 
            if results[i][0][0] == pred_ix and results[i][0][1] == obj_ix:
                gtruth_ix = i
                break
        assert not (gtruth_ix == -1)

        #loss is negative-loss likelihood given by -logp(ground-truth)
        # print results[gtruth_ix][1]
        loss = -math.log(results[gtruth_ix][1])
        #print "loss: "+str(loss)

        #update the parameter

        #compute dl/dencoding. Loss is given by: 
        # loss = -log{p(pred)*p(obj)} + log{ \sum_{pred',obj'} p(pred')*p(obj') }
        #      = -log{p(pred)} - log{p(obj)} + log{ \sum_{pred',obj'} p(pred')*p(obj') }
        # where p(pred) = e^{W_p y + b_p}_{pred} / \sum_{pred'} e^{W_p y + b_p}_{pred'}
        #       p(obj) = p(categ) p(states)
        #   p(categ) = e^{W_c y + b_c}_{categ} / \sum_{categ'} e^{W_c y + b_c}_{categ'}
        #   p(states) = \prod{state} sigmoid(t_state (W_s y + b_s) )_state where t_state = 1 if state is true else -1
        
        error = numpy.zeros((encoding.size, 1)).T
        
        #gradient: -dlogp(pred)/dy - dlogp(categ)/dy - \sum_s { 1/sigmoid(t_s(Wy+b)_s) dsigmoid(t_s(Wy+b)_s)/dy } 
        #         + { 1/\sum_{pred',obj'} p(pred')p(obj') } * \sum_{pred',obj'} p(pred')p(obj') { dlogp(pred')/dy 
        #                           + dlogp(categ')/dy + dlogp(state')/dy } 

        pred_probabilities = self.softmax(encoding, self.W_pred, self.b_pred)
        obj_probabilites = self.softmax(encoding, self.W_obj, self.b_obj) #obj here refers only to object categories
        state_probabilites = self.sigmoid(self.W_state.dot(encoding)   + self.b_state)
        
        obj = env.objList[obj_ix].obj_vec_sparse
        
        #first half, first factor: -dlogp(pred)/dy
        error = error - self.W_pred[pred_ix]
        for pix in range(0, self.num_predicates):
            error = error + (self.W_pred[pix] * pred_probabilities[pix]).T

        #first half, second factor: -dlogp(categ)/dy
        #find category index of this object
        categ_index = -1
        try:
            # categ_index = obj[0:self.num_object_categories].index(1)
            tmp = numpy.nonzero(obj[0:self.num_object_categories])[0]
            if tmp.size:
                categ_index = tmp[0]
            else: 
                categ_index = 0 
        except ValueError:
            raise AssertionError("Object vector must a one corresponding to some category")

        error = error - self.W_obj[categ_index].T
        # error = error - self.W_obj[obj_ix]
        for objix in range(0, self.num_object_categories):
            error = error + (self.W_obj[objix] * obj_probabilites[objix]).T

        #first half, third factor: - \sum_s { 1/sigmoid(t_s(Wy+b)_s) dsigmoid(t_s(Wy+b)_s)/dy } 
        for s in range(0, self.num_state_space):
            if obj[self.num_object_categories + s] == 1:
                error = error -  (self.W_state[s] * (1 - state_probabilites[s]) ).T
            else:
                error = error + (self.W_state[s] * state_probabilites[s] ).T

        errorZ = numpy.zeros((encoding.size, 1)).T #second half
        #second half
        for i in range(0, self.num_predicates): #actions in the environment         
            prob_pred = pred_probabilities[i]

            for obj_index, obj in enumerate(env.objList):   #objects in the environment
            
                errorThis = numpy.zeros((encoding.shape[0], 1)).T
                obj_vec = obj.obj_vec_sparse
                #second half, first factor
                errorThis = errorThis + self.W_pred[i]
                for pix in range(0, self.num_predicates):
                    errorThis = errorThis - self.W_pred[pix] * pred_probabilities[pix]
            
                #second half, second factor
                categ_index = -1
                try:
                    tmp = numpy.nonzero(obj_vec[0:self.num_object_categories])[0]
                    if tmp.size:
                        categ_index = tmp[0]
                    else: 
                        categ_index = 0 
                except ValueError:
                    raise AssertionError("Object vector must a one corresponding to some category")

                errorThis = errorThis + self.W_obj[categ_index]
                for objix in range(0, self.num_object_categories):
                    errorThis = errorThis - self.W_obj[objix] * obj_probabilites[objix]

                #second half, third factor
                prob_state = 1.0
                for s in range(0, self.num_state_space):
                    if obj_vec[self.num_object_categories + s] == 1:
                        prob_state = prob_state * state_probabilites[s]  #check 
                        errorThis = errorThis +  self.W_state[s] * (1 - state_probabilites[s]) 
                    else:
                        prob_state = prob_state * ( 1 - state_probabilites[s]) #check
                        errorThis = errorThis - self.W_state[s] * state_probabilites[s] 

                errorZ = errorZ + errorThis * prob_pred * obj_probabilites[categ_index] * prob_state

        error = error +  (1.0/Z)*errorZ
        
        
        return loss, error


if __name__ == '__main__':
    env_model = Environment()
    env_model.load_environment()
    encoding = numpy.random.randn(250,1)
    get_action_model = decode_action(num_predicates=4, num_object_categories=76, num_state_space=34, dim_encoding=250)
    lr = 0.1
    import pickle 
    a = pickle.load(open('save.p','r'))
    for i in range(3):
        loss = get_action_model.get_loss(a[i], env_model,0,1)
    # for i in range(50):
    #     # print encoding.shape
    #     loss, error = get_action_model.get_loss_and_backprop(encoding, env_model, 0, 1)
    #     print error.T.shape
    #     encoding = encoding - lr*error.T
