import numpy
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

# Set the random number generators' seeds for consistency
SEED = 123
numpy.random.seed(SEED)

class embed_object:
	
    def __init__(self, dense_vector_dim, learning_rate=.05, l2=.05, hot_vector_dim=121, W=numpy.array([])):
    	from embed_environment import dense_dim
        dense_vector_dim = dense_dim
        if W.size:
            self.W = numpy.copy(W)
        else:
            self.W = numpy.random.randn(dense_vector_dim, hot_vector_dim)
    	self.learning_rate = learning_rate
    	self.l2 = l2
    	self.gradW = numpy.zeros((dense_vector_dim, hot_vector_dim))
    	self.input_vector = None
    		
    def calc_object_embedding(self, x):
    	""" Given a one-hot vector representation of an object
            returns dense vector embedding of it.
            param: x is a column vector
            Embedding given by: Wx
        """
        self.input_vector = x #save input for use in backprop
    	return self.W.dot(x)

    def set_input(self, x):
    	""" changes the input which is used for backprop """
    	self.input_vector = x

    def backprop(self, error):
        """ Updates W using error=dloss/dy for y = (W*x)
            param: error is a column vector
                   x is a column vector
            dl/dW_jk = dl/dy dy/dW_jk = \sum_i dl/dyi dyi/dW_jk
                       where dyi/dW_jk = \delta_ij x_k
            \sum_i dl/dyi dyi/dW_jk = \sum_i dl/dyi \delta_ij x_k
            
            dl/dW_jk = dl/dyj x_k which gives us
            dl/dW = dl/dy * x = error * x
        """
        self.gradW = self.gradW + error * (self.input_vector.T)
    	
    def update_W(self):
        """ updates the W using SGD
            W(t+1) = W(t) - learning_rate* (gradW + l2 * W)
        """
    	self.W = self.W - self.learning_rate * (self.gradW + self.l2 * self.W)
    	
    def flushGradient(self):
        """ flushes the gradients
        """
    	self.gradW = self.gradW * 0

if __name__ == '__main__':
    obj = embed_object()
    x = numpy.zeros((121, 1))
    y = obj.get_object_embedding(x)
    print y.shape
