import cPickle as pkl
def create_dict_from_embedding():
	filename='vocab_word2vec.txt'
	outFile = 'cornell.dict.pkl'
	embedding_vec = {}
	with open(filename, 'r') as f:
	        k = 0
	        for line in f:
	            tmp = line.split(':')
	            embedding_vec[tmp[0]] = k
	            k += 1
	pkl.dump( embedding_vec, open( outFile, "wb" ) )

create_dict_from_embedding()